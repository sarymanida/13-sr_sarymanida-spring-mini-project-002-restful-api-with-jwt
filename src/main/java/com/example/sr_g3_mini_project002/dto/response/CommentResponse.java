package com.example.sr_g3_mini_project002.dto.response;

import com.example.sr_g3_mini_project002.model.Comments;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.stream.events.Comment;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentResponse {

    private String caption;
    private String image;
    private int noLike;
    private boolean owner;
    private int userId;
    private String username;
    private List<Comments> comments = new ArrayList<>();
    private int postId;
}
