package com.example.sr_g3_mini_project002.dto.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserLoginResponse {
    private int id;
    private String username;
    private String image;
    private List<String> roles;
    private String token;



}
