package com.example.sr_g3_mini_project002.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserSignupResponse {

    private String fullname;
    private int id;
    private String image;
    private String username;
    private List<String> roles = new ArrayList<>();
}
