package com.example.sr_g3_mini_project002.dto.response;


import com.example.sr_g3_mini_project002.model.Comments;
import com.example.sr_g3_mini_project002.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PostResponse {

    private String caption;
    private List<CommentResponse> comments;
    private int Postid;
    private String image;
    private int noLike;
    private boolean owner;
    private int userId;
    private String username;
}
