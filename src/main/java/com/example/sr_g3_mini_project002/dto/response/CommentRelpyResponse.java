package com.example.sr_g3_mini_project002.dto.response;


import com.example.sr_g3_mini_project002.model.Comments;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.stream.events.Comment;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentRelpyResponse {

    private int id;
    private String content;
    private int userid;
    private int postId;
    private int parentId;
    List<Comments> replies;
}
