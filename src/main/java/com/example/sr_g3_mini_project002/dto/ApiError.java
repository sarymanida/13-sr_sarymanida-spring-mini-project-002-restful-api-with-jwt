package com.example.sr_g3_mini_project002.dto;


import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiError {

    private String message ;
    private HttpStatus status;
}
