package com.example.sr_g3_mini_project002.dto.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class UserLoginRequest {
    private String username;
    private String password;
}
