package com.example.sr_g3_mini_project002.dto.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserSignupRequest {
    private String fullname;
    private String image;
    private String password;
    private String username;
    private List<String> roles;
}
