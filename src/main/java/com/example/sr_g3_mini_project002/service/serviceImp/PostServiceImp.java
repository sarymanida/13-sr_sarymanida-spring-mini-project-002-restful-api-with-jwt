package com.example.sr_g3_mini_project002.service.serviceImp;

import com.example.sr_g3_mini_project002.dto.request.PostRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.dto.response.PostResponse;
import com.example.sr_g3_mini_project002.model.NumOfLike;
import com.example.sr_g3_mini_project002.model.Post;
import com.example.sr_g3_mini_project002.model.Replies;
import com.example.sr_g3_mini_project002.repository.PostRepository;
import com.example.sr_g3_mini_project002.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.stream.events.Comment;
import java.util.List;

@Service
public class PostServiceImp implements PostService {

    @Autowired
    PostRepository postRepository;

    @Override
    public List<Post> findPostByPagination(int limit, int offset, String filter) {
        return postRepository.findPostByPagination(limit, offset, filter);
    }

    @Override
    public int addPost(PostRequest post, int id) {
        return postRepository.addPost(post, id);
    }

    @Override
    public PostResponse findPostById(int id) {
        return postRepository.findPostById(id);
    }

    @Override
    public CommentResponse findPostCommentById(int id) {
        return postRepository.findPostCommentById(id);
    }

    @Override
    public CommentResponse findAll(int id) {
        return postRepository.findAll(id);
    }

    @Override
    public CommentResponse updateComment(String content, int id) {
        return postRepository.updateComment(content, id);
    }

    @Override
    public int deleteComment(int id, int parentId) {
        return postRepository.deleteComment(id,parentId);
    }

    @Override
    public void deletePost(int id) {
        postRepository.deletePost(id);
    }

    @Override
    public int addReplies(Replies replies) {
        return postRepository.addReplies(replies);
    }

    @Override
    public int findAllComment() {
        return postRepository.findAllComment();
    }

    @Override
    public int findPostByCommentId(int id) {
        return postRepository.findPostByCommentId(id);
    }

    @Override
    public int updatePost(PostRequest post, int id) {
        return postRepository.updatePost(post,id);
    }

    @Override
    public int updateCom(String caption, int commentId, int parentId) {
        return postRepository.updateCom(caption, commentId, parentId);
    }

    @Override
    public PostResponse findPostByCommentsId(int id) {
        return postRepository.findPostByCommentsId(id);
    }

    @Override
    public void insertOwner(boolean bool, int id) {
        postRepository.insertOwner(bool, id);
    }

    @Override
    public boolean findBooleanByUserId(int id) {
        return postRepository.findBooleanByUserId(id);
    }

    @Override
    public int react(int postId, int userId , int vote_id) {
        return postRepository.react(postId, userId ,vote_id);
    }

    @Override
    public NumOfLike findPostReacter(int postId, int userId) {
        return postRepository.findPostReacter(postId,userId);
    }

    @Override
    public int updateReact(int voteId, int id) {
        return postRepository.updateReact(voteId,id);
    }

    @Override
    public int deleteReact(int id) {
        return postRepository.deleteReact(id);
    }

    @Override
    public int findReacter(int postId, int userId) {
        return postRepository.findReacter(postId,userId);
    }
}
