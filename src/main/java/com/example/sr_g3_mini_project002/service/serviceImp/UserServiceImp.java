package com.example.sr_g3_mini_project002.service.serviceImp;

import com.example.sr_g3_mini_project002.dto.request.UserRequestUpdate;
import com.example.sr_g3_mini_project002.dto.request.UserSignupRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.dto.response.UserSignupResponse;
import com.example.sr_g3_mini_project002.model.User;
import com.example.sr_g3_mini_project002.model.UserResponse;
import com.example.sr_g3_mini_project002.repository.UserRepository;
import com.example.sr_g3_mini_project002.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImp(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserResponse findUserByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Override
    public UserResponse findByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Override
    public List<UserResponse> getAllUser() {
        return repository.getAllUser();
    }

    @Override
    public int register(UserSignupRequest user) {
        String encryptedPassword = passwordEncoder.encode(user.getPassword());

        // password: $2.dsfkjalkejdjf;lkasjf;ldskajfa;ldskj
        user.setPassword(encryptedPassword);

        return repository.register(user);
    }

    @Override
    public UserSignupResponse findByID(int id) {
        return repository.findByID(id);
    }

    @Override
    public int findIdByUsername(String username) {
        return repository.findIdByUsername(username);
    }

    @Override
    public int addRoles(int userId, int roleId) {
        return repository.addRoles(userId, roleId);
    }

    @Override
    public int getUserId(String name) {


        return repository.getUserId(name);
    }

    @Override
    public int updateAccount(UserRequestUpdate user, int id) {
        return repository.updateAccount(user, id);
    }

    @Override
    public int findPostIdByUserId(int id) {
        return repository.findPostIdByUserId(id);
    }

    @Override
    public CommentResponse findAccount(int id) {
        return repository.findAccount(id);
    }

    @Override
    public int updateStatus(boolean bool, int id) {
        return repository.updateStatus(bool,id);
    }

    @Override
    public void deleterAccount(int id){
        repository.deleterAccount(id);
    }
}
