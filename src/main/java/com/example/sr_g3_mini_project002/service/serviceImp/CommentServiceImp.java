package com.example.sr_g3_mini_project002.service.serviceImp;


import com.example.sr_g3_mini_project002.dto.request.CommentRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.repository.CommentRepository;
import com.example.sr_g3_mini_project002.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImp implements CommentService {

    @Autowired
    CommentRepository commentRepository;

    @Override
    public int addComment(CommentRequest comment, int id, int userId) {
        return commentRepository.addComment(comment, id, userId);
    }

    @Override
    public CommentResponse findCommentById(int id) {
        return commentRepository.findCommentById(id);
    }
}
