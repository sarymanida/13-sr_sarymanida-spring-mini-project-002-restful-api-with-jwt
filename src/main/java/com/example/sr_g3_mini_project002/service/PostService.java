package com.example.sr_g3_mini_project002.service;

import com.example.sr_g3_mini_project002.dto.request.PostRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.dto.response.PostResponse;
import com.example.sr_g3_mini_project002.model.NumOfLike;
import com.example.sr_g3_mini_project002.model.Post;
import com.example.sr_g3_mini_project002.model.Replies;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.xml.stream.events.Comment;
import java.util.List;

@Service
public interface PostService {

    List<Post> findPostByPagination(int limit, int offset, String filter);

    int addPost(PostRequest post, int id);

    PostResponse findPostById(int id);

    CommentResponse findPostCommentById(int id);
    CommentResponse findAll(int id);
    CommentResponse updateComment(String content, int id);

    int deleteComment(int id, int parentId);

    void deletePost(int id);

    int addReplies(Replies replies);

    int findAllComment();

    int findPostByCommentId(int id);

    int updatePost( PostRequest post, int id);

    int updateCom(String caption, int commentId, int parentId);

    PostResponse findPostByCommentsId(int id);
    void insertOwner(boolean bool, int id);

    boolean findBooleanByUserId(int id);

    int react(int postId, int userId, int vote_id);

    NumOfLike findPostReacter(int postId, int userId);

    int updateReact(int voteId,int id);

    int deleteReact(int id);

    int findReacter(int postId,int userId);
}
