package com.example.sr_g3_mini_project002.service;


import com.example.sr_g3_mini_project002.dto.request.CommentRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

@Service
public interface CommentService {
    int addComment(CommentRequest comment, int id, int userId);

    CommentResponse findCommentById(int id);

}
