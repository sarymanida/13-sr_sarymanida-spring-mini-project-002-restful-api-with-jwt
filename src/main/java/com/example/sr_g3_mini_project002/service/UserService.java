package com.example.sr_g3_mini_project002.service;

import com.example.sr_g3_mini_project002.dto.request.UserRequestUpdate;
import com.example.sr_g3_mini_project002.dto.request.UserSignupRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.dto.response.UserSignupResponse;
import com.example.sr_g3_mini_project002.model.User;
import com.example.sr_g3_mini_project002.model.UserResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    UserResponse findUserByUsername(String username);

    UserResponse findByUsername(String username);

    List<UserResponse> getAllUser();

    int register(UserSignupRequest user);
    UserSignupResponse findByID(int id);
    int findIdByUsername(String username);
    int addRoles(int userId, int roleId);

    int getUserId(String name);

    int updateAccount(UserRequestUpdate user, int id);

    int findPostIdByUserId(int id);

    CommentResponse findAccount(int id);

    int updateStatus(boolean bool, int id);

    void deleterAccount(int id);

}
