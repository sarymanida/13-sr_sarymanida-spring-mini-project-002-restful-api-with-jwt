package com.example.sr_g3_mini_project002.repository;


import com.example.sr_g3_mini_project002.dto.request.UserRequestUpdate;
import com.example.sr_g3_mini_project002.dto.request.UserSignupRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.dto.response.UserSignupResponse;
import com.example.sr_g3_mini_project002.model.AuthUser;
import com.example.sr_g3_mini_project002.model.User;
import com.example.sr_g3_mini_project002.model.UserResponse;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
public interface UserRepository {

    @Select("select * from app_users where username =#{username}")
    @Results({
            @Result(property = "roles", column = "id", many = @Many(select = "findRolesById"))
    })
    AuthUser findUserByUsername(String username);

    @Select("select role_name from roles inner join user_roles on user_roles.role_id = roles.id where user_roles.user_id=#{id}")
    List<String> findRolesById(int id);

    @Select("select * from app_users where username=#{username}")
    @Results({
            @Result(property = "roles", column = "id", many = @Many(select = "findRolesById"))
    })
    UserResponse findByUsername(String username);


    @Select("select * from app_users")
    @Results({
            @Result(property = "fullname", column = "name"),
            @Result(property = "roles", column = "id", many = @Many(select = "findRolesById"))
    })
    List<UserResponse> getAllUser();

    @Select("insert into app_users(name, image,password,username) " +
            "values (#{app_users.fullname},#{app_users.image},#{app_users.password}," +
            "#{app_users.username}) returning id ")
    int register(@Param("app_users") UserSignupRequest app_users );

    @Select("select  * from app_users where id=#{id}")
    @Results({
            @Result(property = "fullname", column = "name"),
            @Result(property = "id", column = "id"),
            @Result(property = "roles", column = "id", many = @Many(select = "getRoleByUserId"))
    })
    UserSignupResponse findByID(int id);

    @Select("select distinct role_name from roles inner join user_roles on roles.id = user_roles.role_id where user_roles.user_id=#{id}")
    List<String> getRoleByUserId(int id);

    @Select("select id from app_users where username=#{username}")
    int findIdByUsername(String username);

    @Select("insert into user_roles (user_id, role_id) values (#{userId},#{roleId}) returning user_id")
    int addRoles(int userId, int roleId);

    @Select("select id from app_users where username = #{name} ")
    int getUserId(String name);

    @Select("update app_users set name=#{user.fullname}, image=#{user.imageUrl}, username=#{user.username} where id = #{id} returning id")
    int updateAccount(UserRequestUpdate user, int id);

    @Select("select post_id from post where user_id=#{id}")
    int findPostIdByUserId(int id);

    @Select("select * from post where user_id=#{id}")
    @Results({

            @Result(property = "caption", column = "caption"),
            @Result(property = "image", column = "image"),
            @Result(property = "noLike", column = "no_like"),
            @Result(property = "owner", column = "owner"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "username", column = "username"),
            @Result(property = "comments", column = "post_id", many = @Many(select = "com.example.sr_g3_mini_project002.repository.PostRepository.findCommentByPostId")),
            @Result(property = "postId", column = "post_id")
    })
    CommentResponse findAccount(int id);

    @Select("update set app_users is_enabled=#{bool} where id=#{id}")
    int updateStatus(boolean bool, int id);

    @Select("delete from app_users where id=#{id}")
    void deleterAccount(int id);

}
