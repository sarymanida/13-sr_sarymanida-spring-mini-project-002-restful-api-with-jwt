package com.example.sr_g3_mini_project002.repository;

import com.example.sr_g3_mini_project002.dto.request.CommentRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.dto.response.PostResponse;
import org.apache.ibatis.annotations.*;

@Mapper
public interface CommentRepository {

    @Select("insert into comments (content, post_id, user_id) values (#{comment.content}, #{id}, #{userId}) returning post_id;")
    int addComment(@Param("comment") CommentRequest comment, int id, int userId);

    @Select("select * from comment where comment_id=#{id}")
    CommentResponse findCommentById(int id);
}
