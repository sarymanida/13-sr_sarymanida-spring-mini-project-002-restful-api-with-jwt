package com.example.sr_g3_mini_project002.repository;

import com.example.sr_g3_mini_project002.dto.request.PostRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.dto.response.PostResponse;
import com.example.sr_g3_mini_project002.model.Comments;
import com.example.sr_g3_mini_project002.model.NumOfLike;
import com.example.sr_g3_mini_project002.model.Post;
import com.example.sr_g3_mini_project002.model.Replies;
import com.example.sr_g3_mini_project002.repository.providers.PostProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PostRepository {

    @Select("insert into post (caption, image,user_id) values (#{post.caption}, #{post.image},#{id}) returning post_id")
    @Results({
            @Result(property = "postId", column = "post_id"),
            @Result(property = "user", column = "user_id"),
    })
    int addPost(@Param("post") PostRequest post, int id);

    @SelectProvider(type = PostProvider.class, method ="getAllPost" )
    @Results({
            @Result(property = "caption", column = "caption"),
            @Result(property = "image", column = "image"),
            @Result(property = "noLike", column = "no_like"),
            @Result(property = "owner", column = "owner"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "username", column = "user_id", many = @Many(select = "findUsername")),
            @Result(property = "comments", column = "post_id", many = @Many(select = "findCommentByPostId")),
            @Result(property = "postId", column = "post_id")
    })
    List<Post> findPostByPagination(int limit, int offset, String filter);



    @Select("select * from post inner join app_users on post.user_id = app_users.id where post_id=#{id}")
    @Results({
            @Result(property = "noLike", column = "no_like"),
            @Result(property = "userId", column = "id"),
    })
    PostResponse findPostById(int id);

    @Select("select * from comments inner join post on comments.post_id = post.post_id where comments.post_id =#{id}")
    List<CommentResponse> findCommentById(int id);

    @Select("select * from post inner join comments on post.post_id = comments.post_id where post.post_id = #{id}")
    CommentResponse findPostCommentById(int id);


    @Select("select * from post where post_id=#{id}")
    @Results({

            @Result(property = "caption", column = "caption"),
            @Result(property = "image", column = "image"),
            @Result(property = "noLike", column = "post_id", many = @Many(select = "countReact")),
            @Result(property = "owner", column = "owner"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "username", column = "user_id", many = @Many(select = "findUsername")),
            @Result(property = "comments", column = "post_id", many = @Many(select = "findCommentByPostId")),
            @Result(property = "postId", column = "post_id")
    })
    CommentResponse findAll(int id);

    @Select("select username from app_users where id=#{id}")
    String findUsername(int id);

    @Select("select * from comments where post_id=#{id} and parent_id is null ")
    @Results({
            @Result(property = "id", column = "comment_id"),
            @Result(property = "content", column = "content"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "postId", column = "post_id"),
            @Result(property = "replies", column = "comment_id", many = @Many(select = "findReliesByParentId"))
    })
    List<Comments> findCommentByPostId(int id);

    @Select("select * from comments where parent_id =#{comment_id}")
    @Results({
            @Result(property = "id", column = "comment_id"),
            @Result(property = "content", column = "content"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "parentId", column = "parent_id"),
            @Result(property = "postId", column = "post_id"),
//            @Result(property = "replies", column = "comment_id", many = @Many(select = "findReliesByParentId"))
    })
    List<Replies> findReliesByParentId(int comment_id);


    @Insert("insert into comment (content) values (#{content}) where comment_id = #{id}")
    CommentResponse updateComment(String content, int id);

    @Select("delete from comments where comment_id = #{id} or parent_id =#{parentId} returning post_id")
    int deleteComment(int id, int parentId);

    @Delete("delete from post where post_id = #{id} ")
    void deletePost(int id);

    @Select("insert into comments (content, post_id, parent_id, user_id) values (#{replies.content}, #{replies.postId}, #{replies.parentId}, #{replies.userId}) returning post_id")
    int addReplies(@Param("replies") Replies replies);

    @Select("select comment_id from comments")
    int findAllComment();

    @Select("select post.post_id from post inner join comments on post.post_id = comments.post_id where comments.comment_id=#{id}")
    int findPostByCommentId(int id);

    @Select("update post set caption=#{post.caption}, image=#{post.image} where post_id = #{id} returning post_id")
    int updatePost(@Param("post") PostRequest post, int id);


    @Select("update comments set content=#{caption} where comment_id = #{commentId} and parent_id=#{parentId} returning post_id")
    int updateCom(String caption, int commentId, int parentId);

    @Select("select post.post_id from post inner join comments on post.post_id = comments.post_id where comments.comment_id=#{id}")
    PostResponse findPostByCommentsId(int id);

    @Select("select * post from where post_id =#{id} returning post_id")
    int findPost(int id);

    @Select("select is_enabled from app_users where id=#{id}")
    boolean findBooleanByUserId(int id);

    @Select("update post set owner=#{bool} where post_id = #{id}")
    void insertOwner(boolean bool, int id);

    @Select("insert into post_detail (post_id, user_id,vote_id) values (#{postId},#{userId},#{vote_id}) returning post_id")
    int react(int postId, int userId, int vote_id);

    @Select("select * from post_detail where post_id = #{postId} and user_id=#{userId}")
    NumOfLike findPostReacter(int postId, int userId);
    @Select("select userId from post_detail where post_id = #{postId} and user_id=#{userId}")
    int findReacter(int postId,int userId);

    @Select("update post_detail set vote_id = #{voteId} where post_id = #{id} returning post_id")
    int updateReact(int voteId,int id);

    @Select("delete from post_detail where user_id=#{id} returning post_id")
    int deleteReact(int id);

    @Select("select count(user_id) from post_detail where post_id=#{id}")
    int countReact(int id);
}
