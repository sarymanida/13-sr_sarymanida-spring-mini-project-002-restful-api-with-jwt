package com.example.sr_g3_mini_project002.repository.providers;

import org.apache.ibatis.jdbc.SQL;

public class PostProvider {

    public String getAllPost(int limit, int offset,String filter){

        return new SQL(){{

            System.out.println("Filter value : "+filter);

            if (!filter.isEmpty()){
                System.out.println(" Filter is not empty...");
                SELECT("*");
                FROM("post");
                LIMIT("#{limit}");
                OFFSET("#{offset}");
                WHERE("UPPER (caption) like upper('%'||#{filter}||'%')");
            }else {
                SELECT("*");
                FROM("post");
                LIMIT("#{limit}");
                OFFSET("#{offset}");

            }






        }}.toString();
    }

}
