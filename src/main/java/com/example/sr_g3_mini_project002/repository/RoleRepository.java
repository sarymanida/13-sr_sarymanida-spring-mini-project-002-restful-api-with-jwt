package com.example.sr_g3_mini_project002.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RoleRepository {

    @Select("select role_name from roles inner join user_roles on roles.id = user_roles.role_id where user_id =#{id}")
    List<String> getRoleNameById(int id);


}
