package com.example.sr_g3_mini_project002.controller;

import com.example.sr_g3_mini_project002.dto.request.UserRequestUpdate;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.model.User;
import com.example.sr_g3_mini_project002.security.UserDetailImp;
import com.example.sr_g3_mini_project002.service.CommentService;
import com.example.sr_g3_mini_project002.service.PostService;
import com.example.sr_g3_mini_project002.service.UserService;
import com.example.sr_g3_mini_project002.utils.response.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/account")
public class AccountRestController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @Autowired
    CommentService commentService;

    ModelMapper modelMapper = new ModelMapper();

    @PatchMapping("/{id}/update-profile")
    public Response<CommentResponse> updateAccount(@RequestBody UserRequestUpdate user, @RequestParam(value = "0") int id){
        CommentResponse response = new CommentResponse();

        try {
            int insertID = userService.updateAccount(user,id);
            System.out.println(insertID);

            int userId=userService.findPostIdByUserId(insertID);
            System.out.println(userId);

            CommentResponse findPostByID = postService.findAll(userId);
            System.out.println(findPostByID);
            response = modelMapper.map(findPostByID, CommentResponse.class);
        }catch (Exception e){
            System.out.println("insert exception sql : "+e.getMessage());
            return Response.<CommentResponse>notFound().setPayload(null).setError("no matched id..");
        }

        return Response.<CommentResponse>ok().setPayload(response).setError("Updated account successfully");
    }

    @DeleteMapping ("/{id}/close")
    Response<CommentResponse> disableAccount(@RequestParam(value = "0") int id,
                                             @AuthenticationPrincipal UserDetailImp loginUser){

        CommentResponse response = new CommentResponse();
        try{
            System.out.println("Here is value of the login user : "+loginUser);
            int userId = userService.findIdByUsername(loginUser.getUsername());

            userService.deleterAccount(userId);
        }catch (Exception e){
            System.out.println("insert exception sql : "+e.getMessage());
            return Response.<CommentResponse>notFound().setPayload(null).setError("no matched id..");
        }
        return Response.<CommentResponse>ok().setPayload(null).setError("Disable account successfully");
    }
}
