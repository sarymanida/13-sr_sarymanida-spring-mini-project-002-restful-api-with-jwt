package com.example.sr_g3_mini_project002.controller;

import com.example.sr_g3_mini_project002.dto.RegisterDto;
import com.example.sr_g3_mini_project002.dto.request.UserLoginRequest;
import com.example.sr_g3_mini_project002.dto.request.UserSignupRequest;
import com.example.sr_g3_mini_project002.dto.response.UserLoginResponse;
import com.example.sr_g3_mini_project002.dto.response.UserSignupResponse;
import com.example.sr_g3_mini_project002.model.AuthUser;
import com.example.sr_g3_mini_project002.model.User;
import com.example.sr_g3_mini_project002.model.UserResponse;
import com.example.sr_g3_mini_project002.security.jwt.JwtUtils;
import com.example.sr_g3_mini_project002.service.UserService;
import com.example.sr_g3_mini_project002.utils.response.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthRestController {

    @Autowired
    UserService userService;

    @Autowired
    AuthenticationManager authenticationManager;

    ModelMapper modelMapper = new ModelMapper();

    @PostMapping("/login")
    Response<UserLoginResponse> login (@RequestBody UserLoginRequest request){
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        try{
            Authentication authentication = authenticationManager.authenticate(authenticationToken);
            JwtUtils jwtUtils = new JwtUtils();
            String token = jwtUtils.generateJwtToken(authentication);
            UserLoginResponse response = new UserLoginResponse();
            try{

                UserResponse findUserByUsername = userService.findUserByUsername(request.getUsername());
                System.out.println(findUserByUsername);
                response =modelMapper.map(findUserByUsername,UserLoginResponse.class);
                //response.setUsername(findUserByUsername.getUsername());
            }catch (Exception exception){
                System.out.println("Error when finding the user by username");
                return Response.<UserLoginResponse>exception().setError("Failed to find the user by the provided username !");

            }

            response.setToken(token);

            return Response.<UserLoginResponse>ok().setPayload(response).setError("Login successfullly");
        }catch (Exception exception){
            System.out.println("Exception occur when trying to login for this credential : "+ exception.getMessage());

            return Response.<UserLoginResponse>exception().setError("Failed to perform the authentication due the the exception issues !");

        }

    }

    @PostMapping("/register")
    public Response<UserSignupResponse> register(@RequestBody UserSignupRequest user){
        UserSignupResponse response = new UserSignupResponse();
        try{
            System.out.println(user.getRoles());

            System.out.println("Request Student:"+ user);
            int insertID = userService.register(user);
            System.out.println("insert id : "+insertID);
            UserSignupResponse findStudentByID = userService.findByID(insertID);
            System.out.println(findStudentByID);
            int newId= userService.getUserId(findStudentByID.getUsername());

            user.getRoles().stream().forEach(item->{
                if (item.toLowerCase().contains("admin"))
                    userService.addRoles(newId,1);
                else
                    userService.addRoles(newId,2);
            });

            response = modelMapper.map(findStudentByID,UserSignupResponse.class);

        }catch (Exception exception){

            System.out.println("insert exception sql : "+exception.getMessage());
            if (exception.getMessage().contains("duplicate")){
                return Response.<UserSignupResponse>badRequest()
                        .setPayload(null)
                        .setError("This username is already exist. Try different one.")
                        .setSuccess(false)
                        .setMetadata(null);
            }
        }

            return Response.<UserSignupResponse>ok().setPayload(response).setError("Login successfullly");
    }


}
