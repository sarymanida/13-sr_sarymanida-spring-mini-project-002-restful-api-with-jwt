package com.example.sr_g3_mini_project002.controller;

import com.example.sr_g3_mini_project002.dto.request.CommentRequest;
import com.example.sr_g3_mini_project002.dto.request.PostRequest;
import com.example.sr_g3_mini_project002.dto.request.UserSignupRequest;
import com.example.sr_g3_mini_project002.dto.response.CommentResponse;
import com.example.sr_g3_mini_project002.dto.response.PostResponse;
import com.example.sr_g3_mini_project002.dto.response.UserSignupResponse;
import com.example.sr_g3_mini_project002.model.*;
import com.example.sr_g3_mini_project002.repository.UserRepository;
import com.example.sr_g3_mini_project002.security.UserDetailImp;
import com.example.sr_g3_mini_project002.security.UserDetailsServiceImp;
import com.example.sr_g3_mini_project002.service.CommentService;
import com.example.sr_g3_mini_project002.service.PostService;
import com.example.sr_g3_mini_project002.service.UserService;
import com.example.sr_g3_mini_project002.utils.paging.Paging;
import com.example.sr_g3_mini_project002.utils.response.Response;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.xml.stream.events.Comment;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/posts")
public class PostRestController {

    @Autowired
    UserService userService;

    @Autowired
    PostService postService;

    @Autowired
    CommentService commentService;

    ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/filter")
    public Response<List<Post>> findPost(@RequestParam(defaultValue ="10") int limit, @RequestParam(defaultValue = "1")  int page , @RequestParam(defaultValue = "") String filter){
        List<Post> findAllPost = new ArrayList<>();
        try {

            Paging paging =  new Paging();
            paging.setPage(page);
            int offset = (page -1)* limit;
            paging.setLimit(limit);
            findAllPost = postService.findPostByPagination(limit,offset,filter);
            System.out.println(findAllPost);
            return Response.<List<Post>>ok().setPayload(findAllPost).setError("Students fetched successfully!").setMetadata(paging);

        }catch (Exception ex){
            System.out.println(" Fetching data exception: "+ex.getMessage());
            return Response.<List<Post>>exception().setError("Failed to retrieve the student due the sql exception...");
        }

    }

    @PostMapping("/Create-post")
    public Response<PostResponse> createPost(@AuthenticationPrincipal UserDetailImp loginUser, @RequestBody PostRequest postRequest){

        PostResponse response = new PostResponse();
        // insert
        try{
            System.out.println("Here is value of the login user : "+loginUser);
            int userId = userService.findIdByUsername(loginUser.getUsername());

            System.out.println("Request Student:"+ postRequest);
            int insertID = postService.addPost(postRequest,userId);
            boolean status = postService.findBooleanByUserId(insertID);
            System.out.println(status);
            postService.insertOwner(status,insertID);
            System.out.println("insert id : "+insertID);
            CommentResponse findPostByID = postService.findAll(insertID);
            System.out.println(findPostByID);
            // using modelmapper
            response = modelMapper.map(findPostByID, PostResponse.class);

        }catch (Exception exception){

            System.out.println("insert exception sql : "+exception.getMessage());
        }
        return Response.<PostResponse>ok().setPayload(response).setError("Login successfullly");
    }

    @PostMapping("/{post_id}/comment")
    public Response<CommentResponse> createComment(@RequestParam(defaultValue ="content") String content,
                                                   @RequestParam(defaultValue ="0") int postId,
                                                   @AuthenticationPrincipal UserDetailImp loginUser){
        CommentResponse response = new CommentResponse();
        CommentRequest commentRequest= new CommentRequest();
        commentRequest.setContent(content);
//        commentRequest.setPostId(postId);
        try{
            System.out.println("Here is value of the login user : "+loginUser);
            int userId = userService.findIdByUsername(loginUser.getUsername());

            int insertID = commentService.addComment(commentRequest, postId,userId);
            System.out.println("insert id : "+insertID);

            CommentResponse findPostByID = postService.findAll(insertID);
            System.out.println(findPostByID);

            response = modelMapper.map(findPostByID, CommentResponse.class);

        }catch (Exception exception){

            System.out.println("insert exception sql : "+exception.getMessage());
            return Response.<CommentResponse>notFound()
                    .setPayload(null)
                    .setError("The provided post_id doesn't not exist in the database..")
                    .setMetadata(null);
        }
        return Response.<CommentResponse>ok().setPayload(response).setError("Login successfullly");
    }
    @DeleteMapping("/{comment_id}/delete-comment")
        public Response<CommentResponse> deleteComment(@RequestParam(defaultValue = "6") int commentId,
                @RequestParam(defaultValue = "6") int parentId){
        CommentResponse response = new CommentResponse();
        try{

            System.out.println("ji");
            int insertID = postService.deleteComment(commentId,parentId);
            System.out.println("insert id : "+insertID);

            CommentResponse findPostByID = postService.findAll(insertID);
            System.out.println(findPostByID);
            // using modelmapper
            response = modelMapper.map(findPostByID, CommentResponse.class);

        }catch (Exception exception){

            System.out.println("insert exception sql : "+exception.getMessage());
            return Response.<CommentResponse>notFound()
                    .setPayload(null)
                    .setError("id not found")
                    .setMetadata(null);
        }

        return Response.<CommentResponse>ok().setPayload(response)
                .setError("you have successfullly deleted the comment or reply....")
                .setSuccess(true).setStatus(Response.Status.SUCCESS_DELETE);
    }

    @DeleteMapping("/{id}/delete-post")
    public void deletePost(@RequestParam(defaultValue = "0") int id){
        CommentResponse response = new CommentResponse();
        try{

            postService.deletePost(id);


        }catch (Exception exception){

            System.out.println("insert exception sql : "+exception.getMessage());

            Response.<CommentResponse>notFound()
                    .setPayload(null)
                    .setError("id not found")
                    .setMetadata(null);
            return;

        }

        Response.<CommentResponse>ok().setPayload(response)
                .setError("you have successfullly deleted the post....")
                .setSuccess(true).setStatus(Response.Status.SUCCESS_DELETE);
    }

    @PostMapping("/{comment_id/replay")
    public Response<CommentResponse> addReply(@RequestParam(defaultValue = "0") int commentId,
                         @RequestParam(defaultValue = "0") String content,@AuthenticationPrincipal UserDetailImp loginUser){
        CommentResponse response = new CommentResponse();

        System.out.println(commentId);
        Replies replies = new Replies();
        try{
            int userId = userService.findIdByUsername(loginUser.getUsername());
            int postId = postService.findPostByCommentId(commentId);
            replies.setParentId(commentId);
            replies.setContent(content);
            replies.setUserId(userId);
            replies.setPostId(postId);
            System.out.println(replies);
            int commId= postService.addReplies(replies);
            System.out.println(commId);
            CommentResponse findPostByID = postService.findAll(commId);
            System.out.println(findPostByID);
            response = modelMapper.map(findPostByID, CommentResponse.class);
        }catch (Exception exception){
            System.out.println("insert exception sql : "+exception.getMessage());
            return Response.<CommentResponse>notFound().setPayload(null).setError("no matched id..");
        }
        return Response.<CommentResponse>ok().setPayload(response).setError("Login successfullly");
    }

    @GetMapping("/{id}/view")
    public Response<CommentResponse> viewPost(@RequestParam(defaultValue = "0") int id){
        CommentResponse response = new CommentResponse();
        try{
            CommentResponse findPostById = postService.findAll(id);
            System.out.println(findPostById);
            response = modelMapper.map(findPostById,CommentResponse.class);
        }catch (Exception e){
            System.out.println("insert exception sql : "+e.getMessage());
            return Response.<CommentResponse>notFound().setPayload(null).setError("no matched id..");
        }

        return Response.<CommentResponse>ok().setPayload(response).setError("View Post Successfully");
    }


    @PatchMapping("/{id}/update-post")
    public Response<CommentResponse> updatePost(@RequestBody PostRequest postRequest,
                                                @RequestParam(defaultValue = "0") int id){
        CommentResponse response = new CommentResponse();

        try{
            int insertID = postService.updatePost(postRequest,id);

            CommentResponse findPostByID = postService.findAll(insertID);
            System.out.println(findPostByID);

            response = modelMapper.map(findPostByID, CommentResponse.class);

        }catch (Exception e){
            System.out.println("insert exception sql : "+e.getMessage());
            return Response.<CommentResponse>notFound().setPayload(null).setError("no matched id..");
        }

        return Response.<CommentResponse>ok()
                .setPayload(response).
                setError("Update successfully");
    }


    @PatchMapping("/{comment_id}/update-comment")
    public Response<CommentResponse> updateCommentAndReply(@RequestParam(defaultValue = "0") int commentId,
                                                           @RequestParam(defaultValue = "0") String content,
                                                           @RequestParam(defaultValue = "0") int parentId){

        CommentResponse response = new CommentResponse();

        try{
            int insertID = postService.updateCom(content, commentId, parentId);

            System.out.println(insertID);
            CommentResponse findPostByID = postService.findAll(insertID);
            System.out.println(findPostByID);
            response = modelMapper.map(findPostByID, CommentResponse.class);

        }catch (Exception e){
            System.out.println("insert exception sql : "+e.getMessage());
            return Response.<CommentResponse>notFound().setPayload(null).setError("no matched id..");
        }

        return Response.<CommentResponse>ok().setPayload(response).setError("Update successfully");
    }
    enum TypeReact{
        LIKE
    }
    @PatchMapping("/{id}/react")
    public Response<CommentResponse> reactPost(@RequestParam(defaultValue = "0") int id, @RequestParam TypeReact typeReact,
                                               @AuthenticationPrincipal UserDetailImp loginUser){
        CommentResponse response = new CommentResponse();
        try{
            System.out.println("hi");
            System.out.println("Here is value of the login user : "+loginUser);
            int userId = userService.findIdByUsername(loginUser.getUsername());
            int postID=0;

            NumOfLike postReact = postService.findPostReacter(id,userId);
            System.out.println(postReact);

                if (postReact!=null && postReact.getUser_id()==userId && postReact.getVote_id()>0){
                    postID = postService.deleteReact(userId);
                    System.out.println("hi");
                }else {
                    postID = postService.react(id, userId,1);
                    System.out.println("helo");
                }

            CommentResponse findPostByID = postService.findAll(postID);
            System.out.println(findPostByID);
            response = modelMapper.map(findPostByID, CommentResponse.class);

        }catch (Exception e){
            System.out.println("insert exception sql : "+e.getMessage());
            return Response.<CommentResponse>notFound().setPayload(response).setError("no matched id..");
        }

        return Response.<CommentResponse>ok().setPayload(response).setError("Successfully reacted on the post");
    }
}
