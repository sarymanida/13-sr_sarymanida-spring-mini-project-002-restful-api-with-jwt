package com.example.sr_g3_mini_project002.security;

import com.example.sr_g3_mini_project002.model.AuthUser;
import com.example.sr_g3_mini_project002.model.UserResponse;
import com.example.sr_g3_mini_project002.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImp implements UserDetailsService {


    @Autowired
    UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        AuthUser loginUser = repository.findUserByUsername(username);

        if (loginUser==null)
            throw new UsernameNotFoundException("Sorry, Cannot Find this User ...");

        List<GrantedAuthority> authorities = loginUser.getRoles()
                .stream()
                .map(e-> new SimpleGrantedAuthority(e))
                .collect(Collectors.toList());


        System.out.println(" Here is the authority value : ");
        authorities.stream().forEach(System.out::println);

        return new UserDetailImp(loginUser.getId(), loginUser.getUsername(),loginUser.getPassword(),authorities);

    }

}
