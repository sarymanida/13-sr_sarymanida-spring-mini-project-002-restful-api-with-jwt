package com.example.sr_g3_mini_project002.mapper;

import com.example.sr_g3_mini_project002.dto.RegisterDto;
import com.example.sr_g3_mini_project002.model.User;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface UserMapper {

    // map from reponse -> request
    User dtoToStudentModel(RegisterDto registerDto);
    //map from request -> response
    RegisterDto studentModelToDto(User student);
}
