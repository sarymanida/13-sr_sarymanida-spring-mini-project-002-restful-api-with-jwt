package com.example.sr_g3_mini_project002.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Post {
    private String caption;
    private String image;
    private int noLike;
    private boolean owner;
    private int userId;
    private String username;
    private List<Comments> comments;
    private int postId;
}
