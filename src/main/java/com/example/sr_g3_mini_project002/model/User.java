package com.example.sr_g3_mini_project002.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    private int id;
    private String fullname;
    private String image;
    private String password;
    private String username;
    private boolean is_enabled;
    private List<String> roles;
    private boolean is_locked;
}
