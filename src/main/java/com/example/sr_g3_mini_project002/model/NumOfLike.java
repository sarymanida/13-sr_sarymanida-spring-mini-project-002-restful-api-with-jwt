package com.example.sr_g3_mini_project002.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NumOfLike {

    private int post_id;
    private int vote_id;
    private int user_id;
}
