package com.example.sr_g3_mini_project002.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Replies {

    private int id;
    private int postId;
    private String content;
    private int parentId;
    private int userId;
//    private List<Replies> replies;
}
