package com.example.sr_g3_mini_project002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserResponse {
    private Integer id;
    private String fullname;
    private String image;
    List<String> roles;
    private String username;
}
