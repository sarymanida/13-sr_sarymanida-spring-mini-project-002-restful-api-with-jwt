package com.example.sr_g3_mini_project002;

import com.example.sr_g3_mini_project002.model.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class SrG3MiniProject002Application {

    public static void main(String[] args) {
        SpringApplication.run(SrG3MiniProject002Application.class, args);
    }

}
