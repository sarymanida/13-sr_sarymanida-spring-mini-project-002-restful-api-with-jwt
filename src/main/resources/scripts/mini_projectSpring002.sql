/*
 Navicat Premium Data Transfer

 Source Server         : localhost_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 140002
 Source Host           : localhost:5432
 Source Catalog        : miniProject002SpringSecurity
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140002
 File Encoding         : 65001

 Date: 26/05/2022 19:47:36
*/


-- ----------------------------
-- Sequence structure for app_users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."app_users_id_seq";
CREATE SEQUENCE "public"."app_users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for comments_comment_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."comments_comment_id_seq";
CREATE SEQUENCE "public"."comments_comment_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_detail_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_detail_id_seq";
CREATE SEQUENCE "public"."post_detail_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for post_post_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."post_post_id_seq";
CREATE SEQUENCE "public"."post_post_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for roles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."roles_id_seq";
CREATE SEQUENCE "public"."roles_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_roles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_roles_id_seq";
CREATE SEQUENCE "public"."user_roles_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for vote_vote_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."vote_vote_id_seq";
CREATE SEQUENCE "public"."vote_vote_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for app_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_users";
CREATE TABLE "public"."app_users" (
  "id" int4 NOT NULL DEFAULT nextval('app_users_id_seq'::regclass),
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "image" varchar(255) COLLATE "pg_catalog"."default",
  "password" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "username" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "is_enabled" bool NOT NULL DEFAULT true
)
;

-- ----------------------------
-- Records of app_users
-- ----------------------------

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS "public"."comments";
CREATE TABLE "public"."comments" (
  "comment_id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1
),
  "post_id" int4,
  "content" varchar(255) COLLATE "pg_catalog"."default",
  "parent_id" int4,
  "user_id" int4
)
;

-- ----------------------------
-- Records of comments
-- ----------------------------

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS "public"."post";
CREATE TABLE "public"."post" (
  "post_id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1
),
  "image" varchar(255) COLLATE "pg_catalog"."default",
  "no_like" int4,
  "user_id" int4,
  "caption" varchar(255) COLLATE "pg_catalog"."default",
  "owner" bool
)
;

-- ----------------------------
-- Records of post
-- ----------------------------

-- ----------------------------
-- Table structure for post_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."post_detail";
CREATE TABLE "public"."post_detail" (
  "post_id" int4 NOT NULL,
  "vote_id" int4,
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1
),
  "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of post_detail
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."roles";
CREATE TABLE "public"."roles" (
  "id" int4 NOT NULL DEFAULT nextval('roles_id_seq'::regclass),
  "role_name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO "public"."roles" VALUES (1, 'ROLE_ADMIN');
INSERT INTO "public"."roles" VALUES (2, 'ROLE_USER');

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_roles";
CREATE TABLE "public"."user_roles" (
  "user_id" int4 NOT NULL,
  "role_id" int4 NOT NULL,
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1
)
)
;

-- ----------------------------
-- Records of user_roles
-- ----------------------------

-- ----------------------------
-- Table structure for vote
-- ----------------------------
DROP TABLE IF EXISTS "public"."vote";
CREATE TABLE "public"."vote" (
  "vote_id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1
),
  "vote_count" int4,
  "vote_type" varchar(32) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of vote
-- ----------------------------
INSERT INTO "public"."vote" OVERRIDING SYSTEM VALUE VALUES (1, 1, 'LIKE');
INSERT INTO "public"."vote" OVERRIDING SYSTEM VALUE VALUES (2, 0, 'UNLIKE');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."app_users_id_seq"
OWNED BY "public"."app_users"."id";
SELECT setval('"public"."app_users_id_seq"', 44, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."comments_comment_id_seq"
OWNED BY "public"."comments"."comment_id";
SELECT setval('"public"."comments_comment_id_seq"', 24, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."post_detail_id_seq"
OWNED BY "public"."post_detail"."id";
SELECT setval('"public"."post_detail_id_seq"', 46, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."post_post_id_seq"
OWNED BY "public"."post"."post_id";
SELECT setval('"public"."post_post_id_seq"', 30, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."roles_id_seq"
OWNED BY "public"."roles"."id";
SELECT setval('"public"."roles_id_seq"', 2, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."user_roles_id_seq"
OWNED BY "public"."user_roles"."id";
SELECT setval('"public"."user_roles_id_seq"', 27, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."vote_vote_id_seq"
OWNED BY "public"."vote"."vote_id";
SELECT setval('"public"."vote_vote_id_seq"', 2, true);

-- ----------------------------
-- Uniques structure for table app_users
-- ----------------------------
ALTER TABLE "public"."app_users" ADD CONSTRAINT "app_users_username_key" UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table app_users
-- ----------------------------
ALTER TABLE "public"."app_users" ADD CONSTRAINT "app_users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Auto increment value for comments
-- ----------------------------
SELECT setval('"public"."comments_comment_id_seq"', 24, true);

-- ----------------------------
-- Primary Key structure for table comments
-- ----------------------------
ALTER TABLE "public"."comments" ADD CONSTRAINT "comments_pkey" PRIMARY KEY ("comment_id");

-- ----------------------------
-- Auto increment value for post
-- ----------------------------
SELECT setval('"public"."post_post_id_seq"', 30, true);

-- ----------------------------
-- Primary Key structure for table post
-- ----------------------------
ALTER TABLE "public"."post" ADD CONSTRAINT "post_pkey" PRIMARY KEY ("post_id");

-- ----------------------------
-- Auto increment value for post_detail
-- ----------------------------
SELECT setval('"public"."post_detail_id_seq"', 46, true);

-- ----------------------------
-- Primary Key structure for table post_detail
-- ----------------------------
ALTER TABLE "public"."post_detail" ADD CONSTRAINT "post_detail_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table roles
-- ----------------------------
ALTER TABLE "public"."roles" ADD CONSTRAINT "roles_role_name_key" UNIQUE ("role_name");

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "public"."roles" ADD CONSTRAINT "roles_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Auto increment value for user_roles
-- ----------------------------
SELECT setval('"public"."user_roles_id_seq"', 27, true);

-- ----------------------------
-- Primary Key structure for table user_roles
-- ----------------------------
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "user_roles_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Auto increment value for vote
-- ----------------------------
SELECT setval('"public"."vote_vote_id_seq"', 2, true);

-- ----------------------------
-- Primary Key structure for table vote
-- ----------------------------
ALTER TABLE "public"."vote" ADD CONSTRAINT "vote_pkey" PRIMARY KEY ("vote_id");

-- ----------------------------
-- Foreign Keys structure for table comments
-- ----------------------------
ALTER TABLE "public"."comments" ADD CONSTRAINT "com_par_id" FOREIGN KEY ("parent_id") REFERENCES "public"."comments" ("comment_id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."comments" ADD CONSTRAINT "com_pos_id" FOREIGN KEY ("post_id") REFERENCES "public"."post" ("post_id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."comments" ADD CONSTRAINT "com_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."app_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post
-- ----------------------------
ALTER TABLE "public"."post" ADD CONSTRAINT "po_user_id" FOREIGN KEY ("user_id") REFERENCES "public"."app_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table post_detail
-- ----------------------------
ALTER TABLE "public"."post_detail" ADD CONSTRAINT "po_vo_id" FOREIGN KEY ("post_id") REFERENCES "public"."post" ("post_id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."post_detail" ADD CONSTRAINT "user_po_id" FOREIGN KEY ("user_id") REFERENCES "public"."app_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."post_detail" ADD CONSTRAINT "vo_po_id" FOREIGN KEY ("vote_id") REFERENCES "public"."vote" ("vote_id") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table user_roles
-- ----------------------------
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "user_roles_role_id_fkey" FOREIGN KEY ("role_id") REFERENCES "public"."roles" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "user_roles_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."app_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
